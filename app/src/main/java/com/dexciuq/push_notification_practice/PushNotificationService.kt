package com.dexciuq.push_notification_practice

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

private const val TAG = "PushNotificationService"
private const val CHANNEL_ID = "PushNotificationChannelId"
private const val CHANNEL_NAME = "PushNotificationChannelName"
private const val ID = 555

class PushNotificationService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.i(TAG, "onNewToken: $token}")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }

        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
        )

        Log.i(TAG, "onMessageReceived: ${message.data}")

        val notification = NotificationCompat.Builder(this@PushNotificationService, CHANNEL_ID)
            .setContentTitle(message.notification?.title ?: message.data["title"])
            .setContentText(message.notification?.body ?: message.data["body"])
            .setSmallIcon(R.drawable.ic_logo)
            .setContentIntent(pendingIntent)
            .setVibrate(LongArray(250))
            .build()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationManager.notify(ID, notification)
    }
}